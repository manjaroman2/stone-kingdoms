When adding new tracks, make sure to normalize perceived loudness to approximately -23 LUFS.
It is easy to do it e.g. in Audacity: https://manual.audacityteam.org/man/loudness_normalization.html
