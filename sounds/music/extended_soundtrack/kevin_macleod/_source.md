All tracks were made by Kevin MacLeod (www.incompetech.com)
Licensed under Creative Commons: By Attribution 3.0
https://creativecommons.org/licenses/by/3.0/

---

Title: Kevin MacLeod - Achaidh Cheide
Source URI: https://incompetech.com/music/royalty-free/mp3-royaltyfree/Achaidh%20Cheide.mp3
Source website: https://incompetech.com/music/royalty-free/music.html
Licence: CC BY 3.0
Description: "Instruments: Fiddle, Harp. Whistle, Bouzouki, Dulcimer. Feel: Dark, Driving. Traditional Irish-sounding. I had a hard time classifying this in my existing feels. It feels like Irish folk music."

Title: Kevin MacLeod: Angevin B
Source URI: https://incompetech.com/music/royalty-free/mp3-royaltyfree/Angevin%20B.mp3
Source website: https://incompetech.com/music/royalty-free/music.html
Licence: CC BY 3.0
Description: "Instruments: Flute, Lute, Choir, Percussion. Feel: Dark, Mystical. Faster Renaissance like piece of music. Also available in a very slow version."

Title: Kevin MacLeod: Folk Round
Source URI: https://incompetech.com/music/royalty-free/mp3-royaltyfree/Folk%20Round.mp3
Source website: https://incompetech.com/music/royalty-free/music.html
Licence: CC BY 3.0
Description: "Instruments: Guitar, Violin, Recorder, Piccolo, Percussion, Accordion. Feel: Dark, Eerie, Relaxed. A simple folk perpetual canon - also known as a round. Feels like a medieval campfire piece. "

Title: Kevin MacLeod: Lord of the Land
Source URI: https://incompetech.com/music/royalty-free/mp3-royaltyfree/Lord%20of%20the%20Land.mp3
Source website: https://incompetech.com/music/royalty-free/music.html
Licence: CC BY 3.0
Description: "Instruments: Percussion, Lute, Flute. Feel: Mysterious, Mystical, Suspenseful Middle of the road sort of medieval music... Also good when not travelling on a road."

Title: Kevin MacLeod: Midnight Tale
Source URI: https://incompetech.com/music/royalty-free/mp3-royaltyfree/Midnight%20Tale.mp3
Source website: https://incompetech.com/music/royalty-free/music.html
Licence: CC BY 3.0
Description: "Instruments: Guitar, Lute. Feel: Calm, Relaxed. Gather 'round the tavern's fire and I will share a tale for as long as I have ale. Hah! "Tale and ale!" I'm not guaranteeing rhymes, though. That was a coincidence. I was playing lute for an uninterested group of tavern-goers, when all of a sudden, I remembered that I had a sandwich for lunch! It looks like I'm out of ale, so that is the end of my tale."

Title: Kevin MacLeod: Painting Room
Source URI: https://freepd.com/music/Painting%20Room.mp3
Source website: https://freepd.com/misc.php
Licence: CC0 1.0 Universal
Description: "Written by Kevin MacLeod. Simple, melodic and beautiful song played with gentle and warm celtic harp. This music is available for commercial and non-commercial purposes."

Title: Kevin MacLeod: Pippin the Hunchback
Source URI:  https://incompetech.com/music/royalty-free/mp3-royaltyfree/Pippin%20the%20Hunchback.mp3
Source website: https://incompetech.com/music/royalty-free/music.html
Licence: CC BY 3.0
Description: "Instruments: Guitar, Lute, Flute, Drums. Feel: Bouncy, Mysterious. Understated medieval-type piece. Loopable."

Title: Kevin MacLeod - Skye Cuillin
Source URI: https://incompetech.com/music/royalty-free/mp3-royaltyfree/Skye%20Cuillin.mp3
Source website: https://incompetech.com/music/royalty-free/music.html
Licence: CC BY 3.0
Description: "Instruments: Harp, Whistle, Fiddle, Synths, Strings, Choir. Feel: Calming, Epic, Mystical, Uplifting. New age and Celtic with super round synths and lots of delay. "

Title: Kevin MacLeod: Suonatore di Liuto
Source URI: https://incompetech.com/music/royalty-free/mp3-royaltyfree/Suonatore%20di%20Liuto.mp3
Source website: https://incompetech.com/music/royalty-free/music.html
Licence: CC BY 3.0
Description: "Instruments: Lute. Feel: Mysterious, Relaxed. Simple little background lute piece."

Title: Kevin MacLeod: Teller of the Tales
Source URI:  https://incompetech.com/music/royalty-free/mp3-royaltyfree/Teller%20of%20the%20Tales.mp3
Source website: https://incompetech.com/music/royalty-free/music.html
Licence: CC BY 3.0
Description: "Instruments: Lute, Guitar. Feel: Calming, Mysterious, Relaxed, Somber. Simple subtle plucked duet of ancient-inspired origins."

Title: Kevin MacLeod: The Britons
Source URI: https://freepd.com/music/The%20Britons.mp3
Source website: https://freepd.com/misc.php
Licence: CC0 1.0 Universal
Description: "Written by Kevin MacLeod. Lute? Guitar? Recorder? I don't know. Sounds nice. Seems like a good piece for a medieval RPG utility screen or non-combat area. This music is available for commercial and non-commercial purposes."

Title: Kevin MacLeod - The Pyre
Source URI: https://incompetech.com/music/royalty-free/mp3-royaltyfree/The%20Pyre.mp3
Source website: https://incompetech.com/music/royalty-free/music.html
Licence: CC BY 3.0
Description: "Instruments: Cellos, Mandolin, Percussion, Violins. Feel: Eerie, Epic, Mystical, Somber. Vaguely medieval overtones. The opening section repeats in the second half with violins added."

Title: Kevin MacLeod: Village Consort
Source URI: https://incompetech.com/music/royalty-free/mp3-royaltyfree/Village%20Consort.mp3
Source website: https://incompetech.com/music/royalty-free/music.html
Licence: CC BY 3.0
Description: "Picture a pixellated medieval village... cause that's what this is. It is the kind of music we think they played back in the day. It is not historically. But I'm okay with that, because we also didn't have lich-kings, lich-mages, lich-illithids, lich-warriors, or lich-archers. We did have lichens, though... I like lichens!"
